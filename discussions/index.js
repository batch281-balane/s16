// JS operators

// Arithmetic Operators

let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result of addition operator: " + sum);

let difference = x - y;
console.log("Result of substraction operator: " + difference);

let product = x * y;
console.log("Result of multiplication operator: " + sum);

let quotient = x / y;
console.log("Result of division operator: " + quotient);

let remainder = y % x;
console.log("Result of modulos operator: " + remainder);

//  Assignment Operators (=) - assigns the value of the right operand to a variable

let assignmentNumber = 8;

//  Addition Assignment Operator (+=)

assignmentNumber = assignmentNumber + 2;
console.log("Result of addition assignment operator: " + assignmentNumber);

// Shorthand
assignmentNumber += 2;
console.log("Result of addition assignment operator: " + assignmentNumber);

// Subtraction/Multiplication/Division Assignment Operator (-=, *=, /=)

assignmentNumber -= 2;
console.log("Result of substraction assignment operator: " + assignmentNumber);

assignmentNumber *= 2;
console.log("Result of multiplication assignment operator: " + assignmentNumber);

assignmentNumber /= 2;
console.log("Result of division assignment operator: " + assignmentNumber);

//  Multiple Operator and Parentheses

let mdas  = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);

// Using Parentheses
let pemdas = 1 + (2-3) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas);

pemdas = (1 +(2 - 3)) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas);

// Increment and Decrement

let z = 1;


// pre-increment
let increment = ++z;
console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " + z);

//  post-increment
increment = z++;
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment: " + z);

// pre-decrement
let decrement = --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " + z);

// post-decrement 
decrement = z--;
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement: " + z);

// Type Coercion
let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion)

let numC = true + 1;
console.log(numC);

// Comparison Operators
let juan = 'juan'

// Equality Operator (==)

console.log(1 == 1);
console.log(1 == 2);
console.log(1 == '1');
console.log(0 == false);
console.log('juan' == juan);

// inequality operator (!=)
console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1');
console.log(0 != false);
console.log('juan' != juan);

// Strict Equality operator (===) di tatanggapin kapag di magkaparehas ng data type
console.log(1 === 1);
console.log(1 === 2);
console.log(1 === '1');
console.log(0 === false);
console.log('juan' === juan)

// Strict Inequality operator (!==)
console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== '1');
console.log(0 !== false);
console.log('juan' !== juan);

// Relational Operators 

let a = 50;
let b = 65;

// Greater than (>)
let isGreatherThan = a > b;

// Less Than (<)
let isLessThan = a < b;

// Greater than or equal (>=)
let isGTorEqual = a >= b;

// Less than or equal (<=)
let isLTorEqual = a <= b;


console.log(isGreatherThan);
console.log(isLessThan);
console.log(isGTorEqual);
console.log(isLTorEqual);

// logical operators
let isLegalAge = true;
let isRegistered = false;

// logical AND operator (&&)
// Returns true if all operands are true
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical AND Operator: " + allRequirementsMet);

// Logical OR operator(||)
// Returns true if one of the operands are true
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical OR operator: " + someRequirementsMet);

// Logical Not Operator (!)
// Returns the opposite value
let someRequirementsNotMet = !isRegistered;
console.log("Result of the logical NOT operator: " + someRequirementsNotMet);